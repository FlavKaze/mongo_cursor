from pymongo import MongoClient
from random import choice, randint


class Mongo_example:
    def __init__(self):
        self.mongo_connection = None
        self.mongo_db = None
        self.mongo_collection = None
        self.person = ["joao", "maria", "pedro", "guilherme", "lorivaldo", "natalie", "ligia",
                       "laura", "arthur", "afunso", "claudio", "keila", "julia", "mateus"]
        self.address = ["araras 433", "andorinhas 3994", " ninhos 636", " jade 37", "tuiuiu 873",
                        "gaviao 724", "sabia 94", "tesourinha 73", "paraiso 444", "cacatua 244",
                        "tucano 402"]
        self.pet_name = ["bob", "pixui", "princesa", "daisy", "peach", "mingau", "bartolomeu",
                         "totó", "gato", "doguinho", "au au", "pézinho"]

    def mongo_conn(self):
        if not self.mongo_connection:
            self.mongo_connection = MongoClient('localhost', 27017)

        self.mongo_db = self.mongo_connection.get_database('teste')
        self.mongo_collection = self.mongo_db.get_collection('teste')

    def get_documents(self):

        mongo_cursor = self.mongo_collection.find()

        while mongo_cursor.alive:
            person = mongo_cursor.next()
            print("--------------------------")
            print(f'Name: {person.get("person")}')
            print(f'Address: {person.get("address")}')
            print(f'Pet: {person.get("pet")}')
            print("--------------------------")

    def insert_documents(self):
        for _ in range(0, randint(10, 30)):
            document = dict(person=choice(self.person),
                            address=choice(self.address),
                            pet=choice(self.pet_name))

            self.mongo_collection.insert_one(document)


if __name__ == '__main__':
    mongo_example = Mongo_example()
    mongo_example.mongo_conn()
    mongo_example.insert_documents()
    mongo_example.get_documents()
